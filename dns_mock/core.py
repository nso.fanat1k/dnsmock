import logging
import dnslib
import asyncio
import aiomcache

from faker import Faker

from config import memcached
logger = logging.getLogger(__name__)


class DNSCore:

    def __init__(self):
        self.faker = Faker()
        self._loop = asyncio.get_event_loop()

        # TODO temp fixtures. Need tests!
        asyncio.ensure_future(self._make_domain_fixtures())
        asyncio.ensure_future(self._make_ip_fixtures())

    @property
    def _m_client(self):
        return aiomcache.Client(*memcached['host'], loop=self._loop)

    async def handle_dns_query(self, data: bytes):
        try:
            msg = dnslib.DNSRecord.parse(data)
            logging.debug('Start handling DNS request:\n%s\n', msg)
            return await self.make_reply(msg)

        except asyncio.CancelledError:
            # Someone has canceled handling of msg. Likely timeout has occurred
            return None

        except Exception as exp:
            logger.exception('Handle DNS query failed:\n%s', exp)

    async def make_reply(self, msg: dnslib.DNSRecord):
        try:
            logger.debug('Try to get reply for: %s', msg.q.qname.idna())
            cache_response = await self._m_client.get(
                bytes(msg.q.qname.idna().encode('utf-8'))
            )
        except Exception as exp:
            logger.exception(exp)
            return False

        if not cache_response:
            reply = msg.reply()
            reply.header.rcode = dnslib.RCODE.NXDOMAIN
        else:
            # TODO Need API implementation
            reply = dnslib.DNSRecord.parse(cache_response)
            setattr(reply, 'questions', msg.questions)
            setattr(reply, 'header', msg.header)

        logger.debug('Prepared reply:\n%s\n', reply)
        return reply.pack()

    def _add_a_rr(self, reply):
        rr = dnslib.RR(reply.q.get_qname(),
                       dnslib.QTYPE.A,
                       rdata=dnslib.A(self.faker.ipv4()),
                       ttl=self.faker.pyint(min=1800)
                       )
        reply.add_answer(rr)
        return self

    def _add_aaa_rr(self, reply):
        rr = dnslib.RR(reply.q.get_qname(),
                       dnslib.QTYPE.AAAA,
                       rdata=dnslib.AAAA(self.faker.ipv6()),
                       ttl=self.faker.pyint(min=1800)
                       )
        reply.add_answer(rr)
        return self

    def _add_ptr_rr(self, reply):
        rr = dnslib.RR(reply.q.get_qname(),
                       dnslib.QTYPE.PTR,
                       rdata=dnslib.PTR(self.faker.domain_name()),
                       ttl=self.faker.pyint(min=1800)
                       )
        reply.add_answer(rr)
        return self

    async def _make_domain_fixtures(self):
        domains = ['google.com', 'ya.ru']

        for x in domains:
            res = dnslib.DNSRecord(
                dnslib.DNSHeader(qr=1),
                q=dnslib.DNSQuestion(x)
            )
            self._add_a_rr(res)._add_aaa_rr(res)
            logger.debug('Store lookup:\n%s\n', res)
            await self._m_client.set(f'{x}.'.encode('utf-8'),
                                     res.pack())

    async def _make_ip_fixtures(self):
        domains = ['8.8.8.8', '8.8.8.9']

        for x in domains:
            res = dnslib.DNSRecord(
                dnslib.DNSHeader(qr=1),
                q=dnslib.DNSQuestion(x)
            )
            self._add_ptr_rr(res)
            logger.debug('Store lookup:\n%s\n', res)
            await self._m_client.set(f'{x}.in-addr.arpa.'.encode('utf-8'),
                                     res.pack())
