import logging
import asyncio
import struct
import config

from typing import List, Tuple
from uuid import uuid4
from asyncio import Protocol

from dns_mock.core import DNSCore

logger = logging.getLogger(__name__)
handle_timeout = config.dns_server['handle_timeout']


class BaseDNSProtocol(Protocol):

    def __init__(self, core):
        Protocol.__init__(self)
        self._transport = None
        self._core: DNSCore = core

    def connection_made(self, transport: asyncio.BaseTransport):
        """Called when a connection is made.
        """

        self._transport = transport


class UDPProtocol(BaseDNSProtocol):

    def datagram_received(self, data: bytes, address: Tuple[str, int]):
        """Called when some datagram is received.

        :param data: bytes
        :param address: tuple, (address, port)
        :return: None
        """

        logging.debug('Receive %d bytes from udp:%s:%d', len(data), *address)
        asyncio.ensure_future(self._handle_dns_packet(data, address))

    async def _handle_dns_packet(self, data: bytes, address: Tuple[str, int]):
        """Pass DNS query to Core and wait result

        :param data: bytes, DNS packet
        :param address: tuple, (address, port).
        :return: None
        """

        try:
            # Task will be cancelled when timeout occurred
            reply = await asyncio.wait_for(self._core.handle_dns_query(data),
                                           timeout=handle_timeout)
            if not reply:
                return None

            logger.debug('Send %d bytes to udp:%s:%d', len(reply), *address)
            self._transport.sendto(reply, address)

        except asyncio.TimeoutError:
            logger.error('Timeout occurred when process dns query from: '
                         'udp:%s:%d', *address)

        except Exception as exp:
            logger.exception('DNS query processing failed:\n%s', exp)


class TCPProtocol(BaseDNSProtocol):

    def __init__(self, *args):
        BaseDNSProtocol.__init__(self, *args)

        self._tasks: List[asyncio.Future] = []
        self._conn_uuid = uuid4()
        self._peer = None
        self._nbytes = 0
        self._msg = bytearray()

        self._fut: asyncio.Future = asyncio.Future()
        self._fut.add_done_callback(self._handle_result)

    def _handle_result(self, fut: asyncio.Future):
        """Handle future result.
        """
        try:
            if fut.cancelled():
                return None

            if not fut.result():
                return None

            data = fut.result()
            data = struct.pack(f'!h{len(data)}s', len(data), data)

            logger.info('Send %d bytes, conn_id: %s',
                        len(data),
                        self._conn_uuid)

            self._transport.write(data)

        except Exception as exp:
            logger.exception(exp)

        finally:
            if not self._transport.is_closing():
                logger.info('Close connection, conn_id: %s', self._conn_uuid)
                self._transport.write_eof()
                self._transport.close()

    async def _timer(self, timeout: int = handle_timeout):
        """TCP connection timer function. When timeout is occurred the future
        will be cancelled.
        """

        logger.debug('Start timer, conn_id: %s', self._conn_uuid)
        _, pending = await asyncio.wait({self._fut}, timeout=timeout)

        if not pending:
            # Future has been completed in time. Return None in this case.
            return None

        logger.error('Timeout occurred, conn_id: %s', self._conn_uuid)

        # Stop DNS query processing.
        [t.cancel() for t in self._tasks if not t.done()]

        if not self._fut.done():
            self._fut.cancel()

    async def _handle_dns_packet(self, data: bytes):
        try:
            result = await self._core.handle_dns_query(data)
            if not self._fut.done():
                self._fut.set_result(result)
        except Exception as exp:
            logger.exception('DNS query processing failed, conn_id: %s:\n%s',
                             self._conn_uuid, exp)

    def connection_made(self, transport: asyncio.BaseTransport):
        """Called when a connection is made.
        """

        self._transport = transport
        self._peer = '{}:{}'.format(*transport.get_extra_info('peername'))

        logger.info('New TCP connection: tcp:%s, conn_id: %s',
                    self._peer, self._conn_uuid)

        # Start tcp connection timer
        asyncio.ensure_future(self._timer())

    def eof_received(self):
        """Called when EOF is received
        """

        logger.debug('Receive EOF, conn_id: %s', self._conn_uuid)
        if not self._fut.cancelled():
            self._fut.cancel()

    def data_received(self, data: bytes):
        """Called when some data is received.
        """

        logging.info('Receive %d bytes, conn_id:%s',
                     len(data), self._conn_uuid)

        data = memoryview(data)
        if not self._nbytes:
            # We receive the first chunk of data
            try:
                self._nbytes, = struct.unpack('!h', data[:2].tobytes())
                self._msg = bytearray(data[2:].tobytes())
            except struct.error as exp:
                logger.exception('Receive wrong packet, conn_id %s:\n%s',
                                 self._conn_uuid, exp)
                return self._fut.cancel()

        if self._nbytes > len(self._msg):
            # Need more data. Save this chunk and return.
            return self._msg.extend(data)

        if self._nbytes < len(self._msg):
            # Receive more bytes than we expecting.
            logger.error('DNS query has a big length, conn_id: %s',
                         self._conn_uuid)
            return self._fut.cancel()
        self._tasks.append(
            asyncio.create_task(self._handle_dns_packet(self._msg))
        )


class DNSServer:

    def __init__(self, address):
        self._loop = asyncio.get_event_loop()
        self._address = address
        self.udp_transport = None
        self.tcp_transport = None

    async def start(self):
        """Start DNS server. Start TCP/UDP transport.

        :return: None
        """
        core = DNSCore()

        self.udp_transport, _ = (
            await self._loop.create_datagram_endpoint(
                lambda: UDPProtocol(core), self._address
            )
        )
        self.tcp_transport = (
            await self._loop.create_server(lambda: TCPProtocol(core),
                                           *self._address)
        )

        logger.info('DNS Server serves on: %s:%d', *self._address)

    def stop(self):
        """Gracefully stop DNS server.

        :return: None
        """

        if self.udp_transport:
            self.udp_transport.close()
        if self.tcp_transport:
            self.tcp_transport.close()
