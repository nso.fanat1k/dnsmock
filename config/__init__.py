from os import environ

dns_server = {
    'ip': environ.get('DNS_LISTEN_IP', '0.0.0.0'),
    'port': int(environ.get('DNS_LISTEN_PORT', 53)),
    'handle_timeout': int(environ.get('DNS_HANDLE_TIMEOUT', 1))
}

memcached = {
    'host': (environ.get('MEMCACHED_HOST', 'localhost'),
             int(environ.get('MEMCACHED_PORT', 11211)))
}

logging = {
    'level': environ.get('LOG_LEVEL', 'DEBUG'),
}
