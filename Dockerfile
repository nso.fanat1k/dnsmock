FROM python:3.7-alpine

ENV DEBIAN_FRONTEND=noninteractive \
    TERM=xterm

COPY . /app
WORKDIR /app

RUN pip install --upgrade pip

RUN pip install -r requirements.txt
CMD ["python3", "main.py"]