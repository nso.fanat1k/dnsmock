## Intro 
This is a DNS mock server which will be used for mocking DNS answers.
This project is in develop stage. Now this server is being able to send NXDomain response on
all DNS queries.  

### Requirements

* dnslib >= 0.9.10
* faker >= 1.0.7

### Run

```bash
$ docker-compose up -d
```

### Down

```bash
$ docker-compose down -v
```

### Problems

DNS mock server starts on the 53 udp/tcp port by default.
If you get error like this: `listen tcp 0.0.0.0:53: bind: address already 
in use` try to stop `systemd-resolved.service`

```bash
$ sudo systemctl stop systemd-resolved.service
```