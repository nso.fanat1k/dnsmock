import asyncio
import signal
import logging
import socket
import config

from dns_mock.server import DNSServer

log_fmt = u'%(levelname)-8s [%(module)s:%(lineno)d] %(message)-8s'
logging.basicConfig(format=log_fmt, level=config.logging['level'])
logger = logging.getLogger(__name__)

if __name__ == '__main__':

    loop = asyncio.get_event_loop()

    server = DNSServer((config.dns_server['ip'],
                        config.dns_server['port']))

    def stop_app():
        server.stop()
        loop.stop()

    for sig in (signal.SIGTERM, signal.SIGINT):
        loop.add_signal_handler(sig, stop_app)

    try:
        loop.run_until_complete(server.start())
        loop.run_forever()
    except KeyboardInterrupt:
        stop_app()
    except (PermissionError, socket.error) as exp:
        logger.exception(exp)
        stop_app()
